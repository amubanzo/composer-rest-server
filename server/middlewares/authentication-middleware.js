/**
 * Let's load our dependencies
 */
const strategy = require('passport-jwt').Strategy;
const ExtractJwt = require('passport-jwt').ExtractJwt;


/*
 * Let's create our middleware
 */
const authMiddleware = {};

/**
 * This is our passport-jwt strategy configurator function
 * @param passport the passport module
 * @param secretOrKey the secretOrKey which
 * @param callback our callback function
 */
authMiddleware.configureStrategy = (passport, secretOrKey, callback) => {
    passport.use(new strategy({
        secretOrKey: secretOrKey,
        jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
    }, callback));
};

/**
 * This is our default function callback handler
 * @param jwtPayload the payload extracted from the jwt token
 * @param done
 * @returns {*}
 */
authMiddleware.defaultCallback = (jwtPayload, done) => {
    return done(null, true);
};

/**
 * When invoked, this function will initialise passport authentication through jwt
 * @param passport the passport module
 */
authMiddleware.init = (passport) => {
    const secretOrKey = process.env.secretOrKey;
    authMiddleware.configureStrategy(passport, secretOrKey, authMiddleware.defaultCallback);
};

module.exports = authMiddleware;
