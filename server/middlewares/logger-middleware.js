module.exports = (req, res, next) => {
    console.log(new Date(), `resource:${req.path}`,`method:${req.method}`);
    next();
};
// passportConfigurator = (passport) => {;
//     const secretOrKey = process.env.secretOrKey;
//
//     const configureStrategy = (callback) => {
//         passport.use(new strategy({
//             secretOrKey: secretOrKey,
//             jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
//         }, callback));
//     };
//
//     const defaultCallback = (jwtPayload, next) => {
//         return next(null, true);
//     };
//
//     configureStrategy(defaultCallback);
// };
